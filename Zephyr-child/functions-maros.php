<?php 

/*  CUSTOM TAG WIDGET */

require_once( __DIR__ . '/inc/svg-icons.php');

function widget_custom_tag_cloud($args) {
 
    // Control number of tags to be displayed - 0 no tags
    $args['number'] = 40;
 
    // Tag font unit px, pt, em
    $args['unit'] = 'px';
 
    // Maximum tag text size
    $args['largest'] = 12;
 
    // Minimum tag text size
    $args['smallest'] = 12;
 
    // Outputs our edited widget
    return $args;
}
add_filter( 'widget_tag_cloud_args', 'widget_custom_tag_cloud' );

//Exclude pages from WordPress Search
if (!is_admin()) {
function wpb_search_filter($query) {
if ($query->is_search) {
$query->set('post_type', 'post');
}
return $query;
}
add_filter('pre_get_posts','wpb_search_filter');
}

/* Describe what the code snippet does so you can remember later on */
add_action('wp_head', 'your_function_name');
function your_function_name(){
    /*
    <?php 
    if (!isset($cookie)){ ?>
        gtag('set', 'anonymizeIp', true);
    <?php } ?>
    */
?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-53690194-1"></script>

    <?php $cookie = array_key_exists('cookie_notice_accepted', $_COOKIE)  ? $_COOKIE['cookie_notice_accepted'] : null; ?>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-53690194-1'<?php if (!isset($cookie)){ ?>, { 'anonymize_ip': true }<?php } ?>);
    
    
    </script>
<?php
};
