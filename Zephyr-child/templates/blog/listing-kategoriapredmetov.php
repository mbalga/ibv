<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Output a single blog listing. Universal template that is used by all the possible blog posts listings.
 *
 * (!) $query_args should be filtered before passing to this template.
 *
 * @var $query_args     array Arguments for the new WP_Query. If not set, current global $wp_query will be used instead.
 * @var $layout         string Blog layout: classic / flat / tiles / cards / smallcircle / smallsquare / compact / related / latest
 * @var $columns        int Columns quantity
 * @var $type           string layout type: 'grid' / 'masonry' / 'carousel'
 * @var $metas          array Meta data that should be shown: array('date', 'author', 'categories', 'tags', 'comments')
 * @var $title_size     string Posts Title Size
 * @var $content_type   string Content type: 'excerpt' / 'content' / 'none'
 * @var $show_read_more boolean Show "Read more" link after the excerpt?
 * @var $pagination     string Pagination type: regular / none / ajax / infinite
 * @var $carousel_arrows       bool used in Carousel type
 * @var $carousel_dots         bool used in Carousel type
 * @var $carousel_center       bool used in Carousel type
 * @var $carousel_autoplay     bool used in Carousel type
 * @var $carousel_interval     bool used in Carousel type
 * @var $carousel_slideby      bool used in Carousel type
 * @var $filter         string Filter type: 'none' / 'category'
 * @var $filter_style   string Filter Bar style: 'style_1' / 'style_2' / ... / 'style_N
 * @var $categories     string Comma-separated list of categories slugs to show
 * @var $el_class       string Additional classes that will be appended to the main .w-blog container
 *
 * @action Before the template: 'us_before_template:templates/blog/listing'
 * @action After the template: 'us_after_template:templates/blog/listing'
 * @filter Template variables: 'us_template_vars:templates/blog/listing'
 */


// Variables defaults and filtering

$queried_object = get_queried_object();
$term_id = $queried_object->term_id;

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$args = array(
        'post_type' => 'predmet',
        'posts_per_page' => -1,
        'orderby' => 'title',
		'order' => 'ASC',
		'paged' => $paged,
		'tax_query' => array(
			array(
				'taxonomy' => 'kategoriapredmetov',
				'field' => 'id',
				'terms' => $term_id
			)
		)
    );

	$the_query = new WP_Query( $args );
    $z_index = 49;
    
    ?>
	
		<div class="vsetky-jazdene-vozidla">
            <div class="row">

			<?php while ( $the_query->have_posts() ) {
				$the_query->the_post();

				$id = get_the_ID();
                $title = get_the_title();
                $cena = get_the_excerpt();
                $popis = get_the_content();
                $obrazok = get_the_post_thumbnail_url('medium'); 
            ?>   
                
                <div class="col-lg-4">
                    <div class="before-jeden-predmet">
                        <div class="jeden-predmet" style="z-index: <?php echo $z_index; ?>">
                            <div class="obrazok">
                                <img src="<?php echo $obrazok; ?>"/>
                                <div class="cena">
                                    <span><?php echo $cena; ?></span>
                                </div>
                            </div>
                            <div class="telo-predmetu">
                                <div class="title-predmetu">
                                    <h5><?php echo $title; ?></h5>
                                </div>
                                <div class="popis-predmetu">
                                    <p><?php echo $popis; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<?php $z_index--; ?>
				<?php } ?>

            </div>
        </div>

            <?php
            
            wp_reset_postdata(); 


