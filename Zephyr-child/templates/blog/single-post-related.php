<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Outputs posts related to the current single post
 *
 * @action Before the template: 'us_before_template:templates/blog/single-post-related'
 * @action After the template: 'us_after_template:templates/blog/single-post-related'
 */

if ( ! has_tag() ) {
	return;
}

$tag_ids = wp_get_post_tags( get_the_ID(), array( 'fields' => 'ids' ) );
$query_args = array(
	'tag__in' => $tag_ids,
	'post__not_in' => array( get_the_ID() ),
	'paged' => 1,
	'showposts' => 3,
	'orderby' => 'rand',
	'ignore_sticky_posts' => 1,
	'post_type' => get_post_type(),
);
// Overloading global wp_query to use it in the inner templates
us_open_wp_query_context();
global $wp_query;
$wp_query = new WP_Query( $query_args );

if ( $wp_query->have_posts() ) {
	$template_vars = array(
		'layout' => us_get_option( 'post_related_layout', 'compact' ),
		'type' => 'grid',
		'columns' => 1,
		'img_size' => us_get_option( 'post_related_img_size' ),
		'metas' => array( 'date' ),
		'content_type' => 'none',
		'show_read_more' => FALSE,
		'pagination' => 'none',
	);
	?>
	<section class="l-section podobne-clanky">
	<div class="l-section-h i-cf">		
		<div class="g-cols wpb_row type_default valign_top vc_inner mb-5">
			<div class="vc_col-sm-6 wpb_column vc_column_container">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="wpb_text_column">
							<div class="wpb_wrapper">
								<h2 class="border-pod-vlavo"><?php _e( 'Related Posts', 'us' ) ?></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="vc_col-sm-6 vc_hidden-xs wpb_column vc_column_container">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="w-btn-wrapper align_right">
							<a class="w-btn style_flat color_primary icon_none" href="http://dev.oktodigital.com/ibv/blog/">
								<span class="w-btn-label">Všetky články</span>
								<span class="ripple-container">
									<span class="ripple ripple-on ripple-out"></span>
								</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<?php us_load_template( 'templates/blog/listing', $template_vars ) ?>
	</div>
	</section><?php
}

us_close_wp_query_context();
