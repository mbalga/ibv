<?php
/* Custom functions for IBV. */
require_once( __DIR__ . '/functions-maros.php');
require_once( __DIR__ . '/inc/custom-shortcodes.php');
require_once( __DIR__ . '/inc/dodavatelia-custom-post-type.php');
require_once( __DIR__ . '/inc/options-page.php');
require_once( __DIR__ . '/inc/pozicovna-filter.php');
require_once( __DIR__ . '/inc/cleaning.php');

$upload_dir = wp_upload_dir();
define('url', get_bloginfo('url'));
define('blog', get_bloginfo('name'));
define('template_url', get_template_directory_uri() );
define('template_dir', get_template_directory() );
define('stylesheet_url', get_stylesheet_directory_uri() );

/************************************************************************************/
/* CSS STYLY */
/************************************************************************************/
function custom_wp_enqueue_style(){
    wp_register_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap');

    wp_register_style('font', 'https://use.typekit.net/fna5nob.css');
    wp_enqueue_style('font');

    wp_register_style('okto-custom-matej', stylesheet_url . '/style-matej.css');
    wp_enqueue_style('okto-custom-matej');
}
add_action('wp_enqueue_scripts', 'custom_wp_enqueue_style');

/************************************************************************************/
/* JS SKRIPTY */
/************************************************************************************/
function okto_scripts() {
	wp_register_script( 'grayscale', stylesheet_url . '/js/grayscale.js', array( 'jquery' ), '2018', true );
	wp_enqueue_script( 'grayscale' );

	wp_register_script( 'scroll-to', stylesheet_url . '/js/scroll-to.min.js', array( 'jquery' ), '2018', true );
    wp_enqueue_script( 'scroll-to' );

    wp_register_script( 'bootstrap-js', '//stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js', array( 'jquery' ), '2018', true);
	wp_enqueue_script( 'bootstrap-js' );
    
	wp_register_script( 'cf-messages', stylesheet_url . '/js/cf7-messages.min.js', array( 'jquery' ), '2018', true );
    wp_enqueue_script( 'cf-messages' );	
    
}
add_action( 'wp_enqueue_scripts', 'okto_scripts' );


/*******************************************
 * PRIDANIE FONTAWESOME (PO MIGRACII ZMENIT)
*******************************************/
function fontawesome(){
    echo '<script defer src="https://pro.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-+1nLPoB0gaUktsZJP+ycZectl3GX7wP8Xf2PE/JHrb7X1u7Emm+v7wJMbAcPr8Ge" crossorigin="anonymous"></script>';
    echo '<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js"></script>';
}
add_action( 'wp_footer', 'fontawesome' );

add_image_size( 'dodavatelia-logo', 120, 50 );

