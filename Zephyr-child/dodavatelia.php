<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/*
 * Template Name: Dodávatelia
 * Template Post Type: post, page, product
 */

$us_layout = US_Layout::instance();
get_header();
?>

 <?php  
    $args = array(  'post_type' => 'dodavatelia',
                    'orderby'   => 'title',
                    'order' => 'ASC',
                    'posts_per_page'   => -1,
                );

    $loop = new WP_Query( $args );
?>      

<div class="l-main">
	<div class="l-main-h i-cf">
		
		<main class="l-content" itemprop="mainContentOfPage">

			<section class="l-section head-polygons">
				<div class="l-section-h i-cf dodavatelia">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="txt-c">
                                    <h1 class="border-pod-center">Dodávatelia</h1>
                                </div>
                                <div class="letters" id="letters">
                                    <?php $letters = range('A', 'Z');
                                    foreach ($letters as $one) { ?>
                                        <a href="#<?php echo $one; ?>" class="js-scroll-to"><?php echo $one; ?></a>
                                    <?php } ?>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?> 
                                        <?php 
                                        $str = get_the_title();
                                        $words = explode(' ', $str);
                                        $firstLetter = $words[0][0];
                    /*                    echo "<pre>";
                                        print_r(the_title()); 
                                        echo "</pre>";

                                        
                    */                 
                                       ?>

                                            <div class="col-4 txt-c dodavatel" id="<?php echo $firstLetter; ?>">
                                                <div class="logo cb-loga">
                                                    <?php the_post_thumbnail('dodavatelia-logo'); ?>
                                                </div>
                                                <div class="title">
                                                    <?php the_title(); ?>
                                                </div>
                                                <div class="popis">
                                                    <?php the_excerpt(); ?>
                                                </div>
                                                <div class="stranky">
                                                    <?php the_content(); ?>
                                                </div>
                                            </div>


                                        <?php endwhile;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>


			</section>

		</main>

	</div>
</div> 
<?php
wp_reset_query(); 
get_footer(); ?>

<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("letters");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
