<?php
/* Custom functions for IBV. */
require_once( __DIR__ . '/functions-maros.php');
require_once( __DIR__ . '/inc/custom-shortcodes.php');
require_once( __DIR__ . '/inc/dodavatelia-custom-post-type.php');
require_once( __DIR__ . '/inc/options-page.php');
require_once( __DIR__ . '/inc/pozicovna-filter.php');
require_once( __DIR__ . '/inc/cleaning.php');

$upload_dir = wp_upload_dir();
define('url', get_bloginfo('url'));
define('blog', get_bloginfo('name'));
define('template_url', get_template_directory_uri() );
define('template_dir', get_template_directory() );
define('stylesheet_url', get_stylesheet_directory_uri() );

/************************************************************************************/
/* CSS STYLY */
/************************************************************************************/
function custom_wp_enqueue_style(){
    wp_register_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap');

    wp_register_style('font', 'https://use.typekit.net/fna5nob.css');
    wp_enqueue_style('font');

    wp_register_style('okto-custom-matej', stylesheet_url . '/style-matej.css');
    wp_enqueue_style('okto-custom-matej');
}
add_action('wp_enqueue_scripts', 'custom_wp_enqueue_style');

/************************************************************************************/
/* JS SKRIPTY */
/************************************************************************************/
function okto_scripts() {
	wp_register_script( 'grayscale', stylesheet_url . '/js/grayscale.js', array( 'jquery' ), '2018', true );
	wp_enqueue_script( 'grayscale' );

//	wp_register_script( 'scroll-to', stylesheet_url . '/js/scroll-to.min.js', array( 'jquery' ), '2018', true );
//    wp_enqueue_script( 'scroll-to' );

    wp_register_script( 'bootstrap-js', '//stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js', array( 'jquery' ), '2018', true);
	wp_enqueue_script( 'bootstrap-js' );
    
	wp_register_script( 'cf-messages', stylesheet_url . '/js/cf7-messages.min.js', array( 'jquery' ), '2018', true );
    wp_enqueue_script( 'cf-messages' );	
    
}
add_action( 'wp_enqueue_scripts', 'okto_scripts' );


/*******************************************
 * PRIDANIE FONTAWESOME (PO MIGRACII ZMENIT)
*******************************************/
function fontawesome(){
    echo '<script defer src="https://pro.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-+1nLPoB0gaUktsZJP+ycZectl3GX7wP8Xf2PE/JHrb7X1u7Emm+v7wJMbAcPr8Ge" crossorigin="anonymous"></script>';
    echo '<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js"></script>';
}
add_action( 'wp_footer', 'fontawesome' );

add_image_size( 'dodavatelia-logo', 120, 50 );

/************************************************************************************/
/* DEFER - PRIDANIE DEFER ATRIBUTU */
/************************************************************************************/
/*
function defer_parsing_of_js ( $url ) {
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.js' ) ) return $url;
    return "$url' defer ";
    }
    add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
*/
    // remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

function remove_json_api () {

    // Remove the REST API lines from the HTML Header
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );

    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );

    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );

   // Remove all embeds rewrite rules.
   add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );

}
add_action( 'after_setup_theme', 'remove_json_api' );

function disable_json_api () {

    // Filters for WP-API version 1.x
    add_filter('json_enabled', '__return_false');
    add_filter('json_jsonp_enabled', '__return_false');
  
    // Filters for WP-API version 2.x
    add_filter('rest_enabled', '__return_false');
    add_filter('rest_jsonp_enabled', '__return_false');
  
  }
  add_action( 'after_setup_theme', 'disable_json_api' );

add_filter( 'feed_links_show_comments_feed', '__return_false' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed

