<?php

/**********************************************************************************/
/** HEAD CLEANUP */
/**********************************************************************************/
function okto_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );

	// Disable emojis
	remove_action( 'wp_head', 'print_emoji_detection_script', 7);
	remove_action( 'wp_print_styles', 'print_emoji_styles');
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	
	//odstranenie visual composer verzie
	remove_action('wp_head', array(visual_composer(), 'addMetaData'));

	//odstranenie dns-prefetch
	remove_action('wp_head', 'wp_resource_hints', 2);

	//odstranenie canonial
	remove_action('wp_head', 'rel_canonical');

	//odstranenie shortlinku
	remove_action('wp_head', 'wp_shortlink_wp_head', 10);

} 

add_action( 'init', 'okto_head_cleanup' );

function remove_revslider_meta_tag() {
    return '';
}
add_filter( 'revslider_meta_generator', 'remove_revslider_meta_tag' );