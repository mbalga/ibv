<?php

/****************************************
 * VYPIS NAHODNE DODAVATELOV NA HOMEPAGE
****************************************/
function vypis_dodavatelov(){
    $query = new WP_Query( array('post_type' => 'dodavatelia', 'orderby'   => 'rand', 'posts_per_page' => '12') );
    if ( $query->have_posts() ) : ?>
        <div class="row cb-loga dodavatelia-homepage">
            <div class="col-xl-12">
                <div class="row">
                    <?php while ( $query->have_posts() ) : $query->the_post(); 
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'dodavatelia-logo'); 
                    echo '<div class="col-xl-2 col-md-2 col-sm-2 col-6">';
                    echo '<img src="'.esc_url($featured_img_url).'" />'; 
                    echo '</div>';
                endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    <?php else : ?>
        <?php echo "Nenašiel sa žiadny dodávateľ"; ?>
    <?php endif; ?>
<?php
}
add_shortcode('dodavatelia', 'vypis_dodavatelov');

/****************************************
 * VYPIS STICKY POSTY (ČLÁNKY)
****************************************/
function vypis_sticky_clanky(){
    $sticky = get_option( 'sticky_posts' );
    rsort( $sticky );
    $sticky = array_slice( $sticky, 0, 3 );
    $the_query = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );
    $posts = $the_query->posts;

    $categories_1 = get_the_category($posts[0]->ID);
    $categories_2 = get_the_category($posts[1]->ID);
    $categories_3 = get_the_category($posts[2]->ID);
    $featured_img_url_1 = get_the_post_thumbnail_url($posts[0]->ID,'full'); 
    $featured_img_url_2 = get_the_post_thumbnail_url($posts[1]->ID,'full'); 
    $featured_img_url_3 = get_the_post_thumbnail_url($posts[2]->ID,'full'); 
    
    if($posts[0]){
    $out .= '<div class="container">
                <div class="row sticky-posts">
                    <div class="col-12 col-lg-7 col-sm-12">
                        <div class="first-sticky">
                            <a href="' .get_permalink($posts[0]->ID). '">
                                <img src="'.$featured_img_url_1.'" />
                                <div class="data">
                                    <div class="category-block"><span class="category">' . $categories_1[0]->name . '</span></div>
                                    <div class="date-block"><i class="fal fa-clock date"></i><span class="date" style="margin-left:5px;">' . get_the_date('', $posts[0]->ID) . ' </span></div>
                                    <a href="' .get_permalink($posts[0]->ID). '"><h2 class="title">' . get_the_title($posts[0]->ID) . '</h2></a>
                                </div>
                                <a href="' .get_permalink($posts[0]->ID). '" class="first-sticky-overlay"></a>
                            </a>
                        </div>
                    </div>';
        if($posts[1]){       
        $out .='        <div class="col-12 col-lg-5 col-sm-12">
                            <div class="col-12 col-sm-6 col-lg-12 druhy-clanok">
                               <div class="second-sticky">
                                    <a href="' .get_permalink($posts[1]->ID). '">
                                        <img src="'.$featured_img_url_2.'" />
                                        <div class="data">
                                            <div class="category-block"><span class="category">' . $categories_2[0]->name . '</span></div>
                                            <div class="date-block"><i class="fal fa-clock date"></i><span class="date" style="margin-left:5px;">' . get_the_date('', $posts[1]->ID) . ' </span></div>
                                            <a href="' .get_permalink($posts[1]->ID). '"><h2 class="title">' . get_the_title($posts[1]->ID) . '</h2></a>
                                        </div>
                                        <a href="' .get_permalink($posts[1]->ID). '" class="second-sticky-overlay"></a>
                                    </a>
                                </div>
                            </div>';
        
            if($posts[2]){
                $out .='            <div class="col-12 col-sm-6 col-lg-12 mrg-t-30 treti-clanok">
                                        <div class="second-sticky">
                                            <a href="' .get_permalink($posts[2]->ID). '">
                                                <img src="'.$featured_img_url_3.'" />
                                                <div class="data">
                                                    <div class="category-block"><span class="category">' . $categories_3[0]->name . '</span></div>
                                                    <div class="date-block"><i class="fal fa-clock date"></i><span class="date" style="margin-left:5px;">' . get_the_date('', $posts[2]->ID) . ' </span></div>
                                                    <a href="' .get_permalink($posts[2]->ID). '"><h2 class="title">' . get_the_title($posts[2]->ID) . '</h2></a>
                                                </div>
                                                <a href="' .get_permalink($posts[2]->ID). '" class="second-sticky-overlay"></a>
                                            </a>
                                        </div>
                                    </div>
                                </div>'; 
            }
        }
    $out .= '   </div>
        </div>';  
    } else {
        echo "Nenašiel sa žiadny článok"; 
    }
    
    wp_reset_postdata();
    return $out;  
}
add_shortcode('sticky-posts', 'vypis_sticky_clanky');

function show_breadcrumbs() {
    ob_start();
    us_breadcrumbs();
}
add_shortcode('breadcrumbs', 'show_breadcrumbs');

function PoskytovaneSluzby($atts) {
    if(!empty($atts)){
        $showAll = false;
        $atts = explode(", ", $atts['show']);
    } else { 
        $showAll = true;
    }


    $poradenstvo_title = esc_attr( get_option('poradenstvo_title') );
    $poradenstvo_text = esc_attr( get_option('poradenstvo_text') );
    $poradenstvo_link = esc_attr( get_option('poradenstvo_link') );

    $doprava_title = esc_attr( get_option('doprava_title') );
    $doprava_text = esc_attr( get_option('doprava_text') );
    $doprava_link = esc_attr( get_option('doprava_link') );

    $vizualizacie_title = esc_attr( get_option('vizualizacie_title') );
    $vizualizacie_text = esc_attr( get_option('vizualizacie_text') );
    $vizualizacie_link = esc_attr( get_option('vizualizacie_link') );

    $stavba_title = esc_attr( get_option('stavba_title') );
    $stavba_text = esc_attr( get_option('stavba_text') );
    $stavba_link = esc_attr( get_option('stavba_link') );

    $kupelne_title = esc_attr( get_option('kupelne_title') );
    $kupelne_text = esc_attr( get_option('kupelne_text') );
    $kupelne_link = esc_attr( get_option('kupelne_link') );

    $pozicovna_title = esc_attr( get_option('pozicovna_title') );
    $pozicovna_text = esc_attr( get_option('pozicovna_text') );
    $pozicovna_link = esc_attr( get_option('pozicovna_link') );

    $montaze_title = esc_attr( get_option('montaze_title') );
    $montaze_text = esc_attr( get_option('montaze_text') );
    $montaze_link = esc_attr( get_option('montaze_link') );

    $miesanie_title = esc_attr( get_option('miesanie_title') );
    $miesanie_text = esc_attr( get_option('miesanie_text') );
    $miesanie_link = esc_attr( get_option('miesanie_link') );

    $termovizia_title = esc_attr( get_option('termovizia_title') );
    $termovizia_text = esc_attr( get_option('termovizia_text') );
    $termovizia_link = esc_attr( get_option('termovizia_link') );
    
    $output = "";

    if($showAll) {
        $output .= '<div class="row len-pc">
                    <div class="col-md-4">
                        <a href="' . $poradenstvo_link . '">
                            <div class="box sluzby-poradenstvo">
                                <div class="ikonka">' . do_shortcode( '[poradenstvo]' ) . '</div>
                                <h4>' . $poradenstvo_title . '</h4>
                                <p class="text-box">' . $poradenstvo_text . '</p>
                                <a class="link" href="' . $poradenstvo_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $poradenstvo_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="' . $doprava_link . '">
                            <div class="box doprava">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[doprava]' ) . '</div>
                                <h4>' . $doprava_title . '</h4>
                                <p class="text-box">' . $doprava_text . '</p>
                                <a class="link" href="' . $doprava_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $doprava_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="' . $vizualizacie_link . '">
                            <div class="box vizualizacie">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[vizualizacie-fasad]' ) . '</div>
                                <h4>' . $vizualizacie_title . '</h4>
                                <p class="text-box">' . $vizualizacie_text . '</p>
                                <a class="link" href="' . $vizualizacie_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $vizualizacie_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div> 
               </div>
               <div class="row len-pc">
                    <div class="col-md-4">
                        <a href="' . $stavba_link . '">
                            <div class="box stavba">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[stavba]' ) . '</div>
                                <h4>' . $stavba_title . '</h4>
                                <p class="text-box">' . $stavba_text . '</p>
                                <a class="link" href="' . $stavba_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $stavba_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="' . $kupelne_link . '">
                            <div class="box kupelne">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[kupelne]' ) . '</div>
                                <h4>' . $kupelne_title . '</h4>
                                <p class="text-box">' . $kupelne_text . '</p>
                                <a class="link" href="' . $kupelne_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $kupelne_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="' . $pozicovna_link . '">
                            <div class="box pozicovna">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[pozicovna]' ) . '</div>
                                <h4>' . $pozicovna_title . '</h4>
                                <p class="text-box">' . $pozicovna_text . '</p>
                                <a class="link" href="' . $pozicovna_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $pozicovna_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div> 
               </div>

               <div class="row len-pc">
                    <div class="col-md-4">
                        <a href="' . $montaze_link . '">
                            <div class="box montaze-nove">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[montaze-nove]' ) . '</div>
                                <h4>' . $montaze_title . '</h4>
                                <p class="text-box">' . $montaze_text . '</p>
                                <a class="link" href="' . $montaze_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $montaze_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="' . $miesanie_link . '">
                            <div class="box miesanie">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[miesanie-farieb]' ) . '</div>
                                <h4>' . $miesanie_title . '</h4>
                                <p class="text-box">' . $miesanie_text . '</p>
                                <a class="link" href="' . $miesanie_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $miesanie_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="' . $termovizia_link . '">
                            <div class="box termovizia">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[termovizia]' ) . '</div>
                                <h4>' . $termovizia_title . '</h4>
                                <p class="text-box">' . $termovizia_text . '</p>
                                <a class="link" href="' . $termovizia_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $termovizia_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div> 
               </div>
               
               <div class="row len-mobil-tablet">
                    <div class="col-md-6">
                        <a href="' . $poradenstvo_link . '">
                            <div class="box sluzby-poradenstvo">
                                <div class="ikonka">' . do_shortcode( '[poradenstvo]' ) . '</div>
                                <h4>' . $poradenstvo_title . '</h4>
                                <p class="text-box">' . $poradenstvo_text . '</p>
                                <a class="link" href="' . $poradenstvo_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $poradenstvo_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="' . $doprava_link . '">
                            <div class="box doprava">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[doprava]' ) . '</div>
                                <h4>' . $doprava_title . '</h4>
                                <p class="text-box">' . $doprava_text . '</p>
                                <a class="link" href="' . $doprava_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $doprava_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
               </div>

               <div class="row len-mobil-tablet">
                    <div class="col-md-6">
                        <a href="' . $vizualizacie_link . '">
                            <div class="box vizualizacie">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[vizualizacie-fasad]' ) . '</div>
                                <h4>' . $vizualizacie_title . '</h4>
                                <p class="text-box">' . $vizualizacie_text . '</p>
                                <a class="link" href="' . $vizualizacie_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $vizualizacie_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div> 
                    <div class="col-md-6">
                        <a href="' . $stavba_link . '">
                            <div class="box stavba">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[stavba]' ) . '</div>
                                <h4>' . $stavba_title . '</h4>
                                <p class="text-box">' . $stavba_text . '</p>
                                <a class="link" href="' . $stavba_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $stavba_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
               </div>

               <div class="row len-mobil-tablet">
                    <div class="col-md-6">
                        <a href="' . $kupelne_link . '">
                            <div class="box kupelne">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[kupelne]' ) . '</div>
                                <h4>' . $kupelne_title . '</h4>
                                <p class="text-box">' . $kupelne_text . '</p>
                                <a class="link" href="' . $kupelne_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $kupelne_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="' . $pozicovna_link . '">
                            <div class="box pozicovna">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[pozicovna]' ) . '</div>
                                <h4>' . $pozicovna_title . '</h4>
                                <p class="text-box">' . $pozicovna_text . '</p>
                                <a class="link" href="' . $pozicovna_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $pozicovna_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div> 
               </div>
               
               <div class="row len-mobil-tablet">
                    <div class="col-md-6">
                        <a href="' . $montaze_link . '">
                            <div class="box montaze-nove">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[montaze-nove]' ) . '</div>
                                <h4>' . $montaze_title . '</h4>
                                <p class="text-box">' . $montaze_text . '</p>
                                <a class="link" href="' . $montaze_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $montaze_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="' . $miesanie_link . '">
                            <div class="box miesanie">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[miesanie-farieb]' ) . '</div>
                                <h4>' . $miesanie_title . '</h4>
                                <p class="text-box">' . $miesanie_text . '</p>
                                <a class="link" href="' . $miesanie_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $miesanie_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div> 
               </div>
               <div class="row len-mobil-tablet">
                    <div class="col-md-6">
                        <a href="' . $termovizia_link . '">
                            <div class="box termovizia">
                                <div class="doprava-svg ikonka">' . do_shortcode( '[termovizia]' ) . '</div>
                                <h4>' . $termovizia_title . '</h4>
                                <p class="text-box">' . $termovizia_text . '</p>
                                <a class="link" href="' . $termovizia_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                <div class="hover-foto"></div>
                                <a href="' . $termovizia_link . '"><div class="hover-overlay"></div></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6"></div> 
               </div>';
    } else {
            $output .= '<div class="row len-pc">';
        if (in_array("poradenstvo", $atts)) {
            $output .=      '<div class="col-md-4">
                                <a href="' . $poradenstvo_link . '">
                                    <div class="box sluzby-poradenstvo">
                                        <div class="ikonka">' . do_shortcode( '[poradenstvo]' ) . '</div>
                                        <h4>' . $poradenstvo_title . '</h4>
                                        <p class="text-box">' . $poradenstvo_text . '</p>
                                        <a class="link" href="' . $poradenstvo_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $poradenstvo_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("doprava", $atts)) {
            $output .=      '<div class="col-md-4">
                                <a href="' . $doprava_link . '">
                                    <div class="box doprava">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[doprava]' ) . '</div>
                                        <h4>' . $doprava_title . '</h4>
                                        <p class="text-box">' . $doprava_text . '</p>
                                        <a class="link" href="' . $doprava_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $doprava_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("vizualizacie_fasady", $atts)) {
            $output .=      '<div class="col-md-4">
                                <a href="' . $vizualizacie_link . '">
                                    <div class="box vizualizacie">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[vizualizacie-fasad]' ) . '</div>
                                        <h4>' . $vizualizacie_title . '</h4>
                                        <p class="text-box">' . $vizualizacie_text . '</p>
                                        <a class="link" href="' . $vizualizacie_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $vizualizacie_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("stavba", $atts)) {
            //$output .=  '</div>';
            //$output .=  '<div class="row len-pc">';
            $output .=      '<div class="col-md-4">
                                <a href="' . $stavba_link . '">
                                    <div class="box stavba">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[stavba]' ) . '</div>
                                        <h4>' . $stavba_title . '</h4>
                                        <p class="text-box">' . $stavba_text . '</p>
                                        <a class="link" href="' . $stavba_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $stavba_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("3d_vizualizacia_kupelne", $atts)) {
            $output .=      '<div class="col-md-4">
                                <a href="' . $kupelne_link . '">
                                    <div class="box kupelne">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[kupelne]' ) . '</div>
                                        <h4>' . $kupelne_title . '</h4>
                                        <p class="text-box">' . $kupelne_text . '</p>
                                        <a class="link" href="' . $kupelne_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $kupelne_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("pozicovna", $atts)) {
            $output .=      '<div class="col-md-4">
                                <a href="' . $pozicovna_link . '">
                                    <div class="box pozicovna">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[pozicovna]' ) . '</div>
                                        <h4>' . $pozicovna_title . '</h4>
                                        <p class="text-box">' . $pozicovna_text . '</p>
                                        <a class="link" href="' . $pozicovna_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $pozicovna_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>'; 
            //$output .=  '</div>';
        }
        if (in_array("montaze", $atts)) {
            //$output .=  '<div class="row len-pc">';
            $output .=      '<div class="col-md-4">
                                <a href="' . $montaze_link . '">
                                    <div class="box montaze-nove">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[montaze-nove]' ) . '</div>
                                        <h4>' . $montaze_title . '</h4>
                                        <p class="text-box">' . $montaze_text . '</p>
                                        <a class="link" href="' . $montaze_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $montaze_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("miesanie_farieb", $atts)) {
            $output .=      '<div class="col-md-4">
                                <a href="' . $miesanie_link . '">
                                    <div class="box miesanie">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[miesanie-farieb]' ) . '</div>
                                        <h4>' . $miesanie_title . '</h4>
                                        <p class="text-box">' . $miesanie_text . '</p>
                                        <a class="link" href="' . $miesanie_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $miesanie_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("termovizne_meranie", $atts)) {
            $output .=      '<div class="col-md-4">
                                <a href="' . $termovizia_link . '">
                                    <div class="box termovizia">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[termovizia]' ) . '</div>
                                        <h4>' . $termovizia_title . '</h4>
                                        <p class="text-box">' . $termovizia_text . '</p>
                                        <a class="link" href="' . $termovizia_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $termovizia_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>'; 
        }
            $output .=  '</div>';
                    
            $output .=  '<div class="row len-mobil-tablet">';
        if (in_array("poradenstvo", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $poradenstvo_link . '">
                                    <div class="box sluzby-poradenstvo">
                                        <div class="ikonka">' . do_shortcode( '[poradenstvo]' ) . '</div>
                                        <h4>' . $poradenstvo_title . '</h4>
                                        <p class="text-box">' . $poradenstvo_text . '</p>
                                        <a class="link" href="' . $poradenstvo_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $poradenstvo_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("doprava", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $doprava_link . '">
                                    <div class="box doprava">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[doprava]' ) . '</div>
                                        <h4>' . $doprava_title . '</h4>
                                        <p class="text-box">' . $doprava_text . '</p>
                                        <a class="link" href="' . $doprava_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $doprava_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
            //$output .=  '</div>';
            //$output .=  '<div class="row len-mobil-tablet">';
        }
        if (in_array("vizualizacie_fasady", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $vizualizacie_link . '">
                                    <div class="box vizualizacie">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[vizualizacie-fasad]' ) . '</div>
                                        <h4>' . $vizualizacie_title . '</h4>
                                        <p class="text-box">' . $vizualizacie_text . '</p>
                                        <a class="link" href="' . $vizualizacie_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $vizualizacie_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("stavba", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $stavba_link . '">
                                    <div class="box stavba">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[stavba]' ) . '</div>
                                        <h4>' . $stavba_title . '</h4>
                                        <p class="text-box">' . $stavba_text . '</p>
                                        <a class="link" href="' . $stavba_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $stavba_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
            //$output .=  '</div>';
        
            //$output .=  '<div class="row len-mobil-tablet">';
        }
        if (in_array("3d_vizualizacia_kupelne", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $kupelne_link . '">
                                    <div class="box kupelne">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[kupelne]' ) . '</div>
                                        <h4>' . $kupelne_title . '</h4>
                                        <p class="text-box">' . $kupelne_text . '</p>
                                        <a class="link" href="' . $kupelne_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $kupelne_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("pozicovna", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $pozicovna_link . '">
                                    <div class="box pozicovna">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[pozicovna]' ) . '</div>
                                        <h4>' . $pozicovna_title . '</h4>
                                        <p class="text-box">' . $pozicovna_text . '</p>
                                        <a class="link" href="' . $pozicovna_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $pozicovna_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>'; 
            //$output .=  '</div>';
                    
            //$output .=  '<div class="row len-mobil-tablet">';
        }
        if (in_array("montaze", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $montaze_link . '">
                                    <div class="box montaze-nove">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[montaze-nove]' ) . '</div>
                                        <h4>' . $montaze_title . '</h4>
                                        <p class="text-box">' . $montaze_text . '</p>
                                        <a class="link" href="' . $montaze_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $montaze_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
        if (in_array("miesanie_farieb", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $miesanie_link . '">
                                    <div class="box miesanie">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[miesanie-farieb]' ) . '</div>
                                        <h4>' . $miesanie_title . '</h4>
                                        <p class="text-box">' . $miesanie_text . '</p>
                                        <a class="link" href="' . $miesanie_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $miesanie_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
            //$output .=  '</div>';
            //$output .=   '<div class="row len-mobil-tablet">';
        }
        if (in_array("termovizne_meranie", $atts)) {
            $output .=      '<div class="col-md-6">
                                <a href="' . $termovizia_link . '">
                                    <div class="box termovizia">
                                        <div class="doprava-svg ikonka">' . do_shortcode( '[termovizia]' ) . '</div>
                                        <h4>' . $termovizia_title . '</h4>
                                        <p class="text-box">' . $termovizia_text . '</p>
                                        <a class="link" href="' . $termovizia_link . '">Viac <i class="fas fa-long-arrow-right"></i></a>
                                        <div class="hover-foto"></div>
                                        <a href="' . $termovizia_link . '"><div class="hover-overlay"></div></a>
                                    </div>
                                </a>
                            </div>';
        }
            $output .=      '<div class="col-md-6"></div>'; 
            $output .=  '</div>';
    }

    return $output;

}
add_shortcode('poskytovane-sluzby', 'PoskytovaneSluzby');

function predmety_pozicovna() {


	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$args = array(
        'post_type' => 'predmet',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'paged' => $paged
    );

	$the_query = new WP_Query( $args );
    $z_index = 49;
    /***************
     * KATEGÓRIE
    ***************/
    $args = array( 'taxonomy' => 'kategoriapredmetov' );
    $terms = get_terms('kategoriapredmetov', $args);
    $count = count($terms); 
    $term_list = "";
    
    if ($count > 0) {
        $term_list .= '<ul class="kategorie-predmetov list-predmetov zoznam-kategorii">';
        $term_list .= '<li><a class="kategoriapredmetov ajax js-scroll-to current" onclick="term_ajax_get(-1);" href="#vsetky-produkty">Všetky</a></li>';  

        foreach ($terms as $term) {
            $term_list .= '<li><a class="kategoriapredmetov ajax js-scroll-to" onclick="term_ajax_get('.$term->term_id.');" href="#vsetky-produkty">'.$term->name.'</a></li>';
        }
        $term_list .= '</ul>';
        echo $term_list;
    }
    ?>
		<div id="vsetky-produkty" class="vsetky-jazdene-vozidla">
            <div class="row">
            <div id="loader"></div>

			<?php while ( $the_query->have_posts() ) {
				$the_query->the_post();

                $id = get_the_ID();
                $title = get_the_title();
                $cena = get_the_excerpt();
                $popis = get_the_content();
                $obrazok = get_the_post_thumbnail_url($id,'medium'); 
            ?>   
                
                <div class="col-lg-4">
                    <div class="before-jeden-predmet">
                        <div class="jeden-predmet" style="z-index: <?php echo $z_index; ?>">
                            <div class="obrazok">
                                <img src="<?php echo $obrazok; ?>"/>
                                <div class="cena">
                                    <span><?php echo $cena; ?></span>
                                </div>
                            </div>
                            <div class="telo-predmetu">
                                <div class="title-predmetu">
                                    <h5><?php echo $title; ?></h5>
                                </div>
                                <div class="popis-predmetu">
                                    <p><?php echo $popis; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<?php $z_index--; ?>
				<?php } ?>

            </div>
        </div>

            <?php
            
            wp_reset_postdata(); 

	 
}
add_shortcode( 'predmety-pozicovna', 'predmety_pozicovna' );