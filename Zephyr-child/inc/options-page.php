<?php 
add_action('admin_menu', function() {
    add_menu_page( 'Nastavenia služieb', 'Nastavenia služieb', 'manage_options', 'sluzby-options', 'sluzbyopt' );
});
 
 
add_action( 'admin_init', function() {
    // Poradenstvo
    register_setting( 'sluzby-options', 'poradenstvo_title' );
    register_setting( 'sluzby-options', 'poradenstvo_text' );
    register_setting( 'sluzby-options', 'poradenstvo_link' );

    // Doprava
    register_setting( 'sluzby-options', 'doprava_title' );
    register_setting( 'sluzby-options', 'doprava_text' );
    register_setting( 'sluzby-options', 'doprava_link' );

    // Vizualizácie
    register_setting( 'sluzby-options', 'vizualizacie_title' );
    register_setting( 'sluzby-options', 'vizualizacie_text' );
    register_setting( 'sluzby-options', 'vizualizacie_link' );

    // Stavba
    register_setting( 'sluzby-options', 'stavba_title' );
    register_setting( 'sluzby-options', 'stavba_text' );
    register_setting( 'sluzby-options', 'stavba_link' );

    // Montáže
    register_setting( 'sluzby-options', 'montaze_title' );
    register_setting( 'sluzby-options', 'montaze_text' );
    register_setting( 'sluzby-options', 'montaze_link' );

    // Požičovňa
    register_setting( 'sluzby-options', 'pozicovna_title' );
    register_setting( 'sluzby-options', 'pozicovna_text' );
    register_setting( 'sluzby-options', 'pozicovna_link' );

});
function sluzbyopt() {
  ?>
    <div class="wrap">
      <form action="options.php" method="post">
 
        <?php
          settings_fields( 'sluzby-options' );
          do_settings_sections( 'sluzby-options' );
        ?>
        <h1>Nastavenia služieb</h1>
        <table>
             
           <h2>Poradenstvo</h2>
            <tr>
                <th>Nadpis</th>
                <td><input type="text" placeholder="Poradenstvo" name="poradenstvo_title" value="<?php echo esc_attr( get_option('poradenstvo_title') ); ?>" size="100" /></td>
            </tr>
            <tr>
                <th>Text</th>
                <td><textarea placeholder="Text" name="poradenstvo_text" rows="2" cols="100"><?php echo esc_attr( get_option('poradenstvo_text') ); ?></textarea></td>
            </tr>

            <tr>
                <th>Odkaz</th>
                <td><input type="text" placeholder="Odkaz" name="poradenstvo_link" value="<?php echo esc_attr( get_option('poradenstvo_link') ); ?>" size="100" /></td>
            </tr>

            <tr>
                <td><?php submit_button(); ?></td>
            </tr>
 
        </table>

        <table>
             
           <h2>Doprava</h2>
            <tr>
                <th>Nadpis</th>
                <td><input type="text" placeholder="Doprava" name="doprava_title" value="<?php echo esc_attr( get_option('doprava_title') ); ?>" size="100" /></td>
            </tr>
            <tr>
                <th>Text</th>
                <td><textarea placeholder="Text" name="doprava_text" rows="2" cols="100"><?php echo esc_attr( get_option('doprava_text') ); ?></textarea></td>
            </tr>

            <tr>
                <th>Odkaz</th>
                <td><input type="text" placeholder="Odkaz" name="doprava_link" value="<?php echo esc_attr( get_option('doprava_link') ); ?>" size="100" /></td>
            </tr>

            <tr>
                <td><?php submit_button(); ?></td>
            </tr>
 
        </table>

        <table>
             
           <h2>Vizualizácie</h2>
            <tr>
                <th>Nadpis</th>
                <td><input type="text" placeholder="Vizualizácie" name="vizualizacie_title" value="<?php echo esc_attr( get_option('vizualizacie_title') ); ?>" size="100" /></td>
            </tr>
            <tr>
                <th>Text</th>
                <td><textarea placeholder="Text" name="vizualizacie_text" rows="2" cols="100"><?php echo esc_attr( get_option('vizualizacie_text') ); ?></textarea></td>
            </tr>

            <tr>
                <th>Odkaz</th>
                <td><input type="text" placeholder="Odkaz" name="vizualizacie_link" value="<?php echo esc_attr( get_option('vizualizacie_link') ); ?>" size="100" /></td>
            </tr>

            <tr>
                <td><?php submit_button(); ?></td>
            </tr>
 
        </table>

        <table>
             
           <h2>Stavba</h2>
            <tr>
                <th>Nadpis</th>
                <td><input type="text" placeholder="Stavba" name="stavba_title" value="<?php echo esc_attr( get_option('stavba_title') ); ?>" size="100" /></td>
            </tr>
            <tr>
                <th>Text</th>
                <td><textarea placeholder="Text" name="stavba_text" rows="2" cols="100"><?php echo esc_attr( get_option('stavba_text') ); ?></textarea></td>
            </tr>

            <tr>
                <th>Odkaz</th>
                <td><input type="text" placeholder="Odkaz" name="stavba_link" value="<?php echo esc_attr( get_option('stavba_link') ); ?>" size="100" /></td>
            </tr>

            <tr>
                <td><?php submit_button(); ?></td>
            </tr>
 
        </table>

        <table>
             
           <h2>Montáže</h2>
            <tr>
                <th>Nadpis</th>
                <td><input type="text" placeholder="Montáže" name="montaze_title" value="<?php echo esc_attr( get_option('montaze_title') ); ?>" size="100" /></td>
            </tr>
            <tr>
                <th>Text</th>
                <td><textarea placeholder="Text" name="montaze_text" rows="2" cols="100"><?php echo esc_attr( get_option('montaze_text') ); ?></textarea></td>
            </tr>

            <tr>
                <th>Odkaz</th>
                <td><input type="text" placeholder="Odkaz" name="montaze_link" value="<?php echo esc_attr( get_option('montaze_link') ); ?>" size="100" /></td>
            </tr>

            <tr>
                <td><?php submit_button(); ?></td>
            </tr>
 
        </table>

        <table>
             
           <h2>Požičovňa</h2>
            <tr>
                <th>Nadpis</th>
                <td><input type="text" placeholder="Požičovňa" name="pozicovna_title" value="<?php echo esc_attr( get_option('pozicovna_title') ); ?>" size="100" /></td>
            </tr>
            <tr>
                <th>Text</th>
                <td><textarea placeholder="Text" name="pozicovna_text" rows="2" cols="100"><?php echo esc_attr( get_option('pozicovna_text') ); ?></textarea></td>
            </tr>

            <tr>
                <th>Odkaz</th>
                <td><input type="text" placeholder="Odkaz" name="pozicovna_link" value="<?php echo esc_attr( get_option('pozicovna_link') ); ?>" size="100" /></td>
            </tr>

            <tr>
                <td><?php submit_button(); ?></td>
            </tr>
 
        </table>
       
      </form>
    </div>
  <?php
}