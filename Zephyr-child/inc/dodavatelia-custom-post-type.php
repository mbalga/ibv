<?php

/**********************************************************************************/
/** CUSTOM POST TYPE */
/**********************************************************************************/
add_action( 'init', 'dodavatelia' );
function dodavatelia() {
    $labels = array(
        'name'               => _x('Dodávatelia', 'post type general name'),
        'singular_name'      => _x('Dodávatelia', 'post type singular name'),
        'add_new'            => _x('Pridať nového dodávateľa', 'prislusenstvo'),
        'add_new_item'       => __('Pridať nového dodávateľa'),
        'edit_item'          => __('Upraviť dodávateľa'),
        'new_item'           => __('Nový dodávateľ'),
        'all_items'          => __('Všetci dodávatelia'),
        'view_item'          => __('Prezerať dodávateľov'),
        'search_items'       => __('Vyhľadať v dodávateľoch'),
        'not_found'          =>  __('žiadni dodávatelia'),
        'not_found_in_trash' => __('žiadni dodávatelia v koši'),
        'parent_item_colon'  => '',
        'menu_name'          => 'Dodávatelia'
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'exclude_from_search' => true,
        'supports'           => array( 'title', 'thumbnail', 'editor', 'excerpt' ),
    );
    register_post_type('dodavatelia',$args);
}

/**********************************************************************************/
/** REGISTER CUSTOM TAXONOMY */
/**********************************************************************************/
function custom_taxonomy() {
    $labels = array(
        'name'                       => _x( 'Kategória predmetov', 'Značky General Name', 'bonestheme' ),
        'singular_name'              => _x( 'Kategória predmetov', 'Značky Singular Name', 'bonestheme' ),
        'menu_name'                  => __( 'Kategória predmetov', 'bonestheme' ),
        'all_items'                  => __( 'Všetky predmety', 'bonestheme' ),
        'parent_item'                => __( 'Parent Item', 'bonestheme' ),
        'parent_item_colon'          => __( 'Parent Item:', 'bonestheme' ),
        'new_item_name'              => __( 'New Item Name', 'bonestheme' ),
        'add_new_item'               => __( 'Add New Item', 'bonestheme' ),
        'edit_item'                  => __( 'Edit Item', 'bonestheme' ),
        'update_item'                => __( 'Update Item', 'bonestheme' ),
        'view_item'                  => __( 'View Item', 'bonestheme' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'bonestheme' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'bonestheme' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'bonestheme' ),
        'popular_items'              => __( 'Popular Items', 'bonestheme' ),
        'search_items'               => __( 'Search Items', 'bonestheme' ),
        'not_found'                  => __( 'Not Found', 'bonestheme' ),
        'no_terms'                   => __( 'No items', 'bonestheme' ),
        'items_list'                 => __( 'Items list', 'bonestheme' ),
        'items_list_navigation'      => __( 'Items list navigation', 'bonestheme' ),
    );

    $capabilities = array(
        'manage_terms'               => 'manage_categories',
        'edit_terms'                 => 'manage_categories',
        'delete_terms'               => 'manage_categories',
        'assign_terms'               => 'edit_posts',
    );

    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'query_var'                  => true,
        'capabilities'               => $capabilities,
        'rewrite' 					 => array(
                                        'slug'         => 'predmet/kategoriapredmetov',
                                        'with_front'   => false,
        ),
    );
    register_taxonomy( 'kategoriapredmetov', array( 'predmet' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );

/**********************************************************************************/
/** CUSTOM POST TYPE */
/**********************************************************************************/
add_action( 'init', 'pozicovnaInit' );
function pozicovnaInit() {
    $labels = array(
        'name'               => _x('Požičovňa', 'kategoriapredmetov'),
        'singular_name'      => _x('Požičovňa', 'kategoriapredmetov'),
        'add_new'            => _x('Pridať nový', 'kategoriapredmetov'),
        'add_new_item'       => __('Pridať nový predmet'),
        'edit_item'          => __('Upraviť predmety'),
        'new_item'           => __('Nový predmet'),
        'all_items'          => __('Všetky predmety'),
        'view_item'          => __('Prezerať predmety'),
        'search_items'       => __('Vyhľadať v predmetoch'),
        'not_found'          =>  __('Žiadne predmety'),
        'not_found_in_trash' => __('Žiadne predmety v koši'),
        'parent_item_colon'  => '',
        'menu_name'          => 'Požičovňa'
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'predmet', 'with_front' => false ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => null,
        'supports'           => array( 'title', 'thumbnail', 'editor', 'excerpt' ),
        'taxonomies'         => array( 'kategoriapredmetov' ),
    );
    register_post_type('predmet', $args);
}