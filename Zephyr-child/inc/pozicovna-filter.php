<?php
add_action( 'wp_ajax_nopriv_load-filter2', 'prefix_load_term_posts' );
add_action( 'wp_ajax_load-filter2', 'prefix_load_term_posts' );
function prefix_load_term_posts () {
        $term_id = $_POST[ 'term' ];
            if($term_id == -1){ $all = true; } else { $all = false; }
            $args = array (
            'post_type' => 'predmet',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'kategoriapredmetov',
                    'field'    => 'id',
                    'terms'    => $term_id,
                    'operator' => $all ? 'NOT IN' : 'IN'
                    )
                )
            
             );

        global $post;
        $myposts = get_posts( $args );
        ob_start (); 
        $z_index = 49;
        ?>

        <div class="row">
        <div id="loader"></div>
        <?php foreach( $myposts as $post ) : setup_postdata($post); ?>
            <?php
				$id = get_the_ID();
                $title = get_the_title();
                $cena = get_the_excerpt();
                $popis = get_the_content();
                $obrazok = get_the_post_thumbnail_url($id,'full'); 
                ?>   
                
                <div class="col-lg-4">
                    <div class="before-jeden-predmet">
                        <div class="jeden-predmet" style="z-index: <?php echo $z_index; ?>">
                            <div class="obrazok">
                                <img src="<?php echo $obrazok; ?>"/>
                                <div class="cena">
                                    <span><?php echo $cena; ?></span>
                                </div>
                            </div>
                            <div class="telo-predmetu">
                                <div class="title-predmetu">
                                    <h5><?php echo $title; ?></h5>
                                </div>
                                <div class="popis-predmetu">
                                    <p><?php echo $popis; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<?php $z_index--; ?>
				
                <?php endforeach; ?>
            </div>

       <?php wp_reset_postdata(); 
       $response = ob_get_contents();
       ob_end_clean();
       echo $response;
       die(1);
}
?>

<?php
