jQuery(document).ready(function ($) {

    // Generic selector to be used anywhere
    $(".js-scroll-to").click(function (e) {

        // Get the href dynamically
        var destination = $(this).attr('href');

        // Prevent href=“#” link from changing the URL hash (optional)
        e.preventDefault();

        // Animate scroll to destination
        $('html, body').animate({
            scrollTop: $(destination).offset().top - 150
        }, 500);
    });



});

function term_ajax_get(termID) {
    jQuery("a.ajax").removeClass("current");
    jQuery("a.ajax").click(function () {
        jQuery(this).addClass("current");
    });

    jQuery("#loading-animation").show();

    var ajaxurl = 'http://dev.oktodigital.com/ibv/engine/wp-admin/admin-ajax.php';
    jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: { "action": "load-filter2", term: termID },
        beforeSend: function () {
            jQuery(".vsetky-jazdene-vozidla .col-lg-4").hide();
            jQuery('#loader').show();
        },
        complete: function () { 
            jQuery('#loader').hide();
        },
        success: function (response) {
            jQuery(".vsetky-jazdene-vozidla").fadeIn("slow").html(response);
            jQuery("#loading-animation").hide();
            return false;
        }
    });
}