jQuery(document).ready(function ($) {

    // nastav si div, kde sa formular nachadza (ak nezadas presne div, zoberie prvy CF7 formular)
    var wpcf7Elm = document.querySelector('.newsletter .wpcf7');

    wpcf7Elm.addEventListener('wpcf7submit', function (event) {
        // Ak zadal niečo zle
        if('validation_failed' == event.detail.status) {

            $('.wpcf7-not-valid-tip').hide();
            var err_length = event.detail.apiResponse.invalidFields.length;

            for (i = 0; i < err_length; i++) { 
                var input = event.detail.apiResponse.invalidFields[i].into;
                var err_message = event.detail.apiResponse.invalidFields[i].message;

                //console.log(event.detail.apiResponse.invalidFields[i].message);
                $(input).css('border', '1px solid #E62444');
                $(input).attr('data-toggle', 'tooltip');
                $(input).attr('data-placement', 'top');
                $(input).attr('data-original-title', '<i class="far fa-times"></i>' + err_message + '');
                $(input).attr('title', '<i class="far fa-times"></i>' + err_message + '');
                $(input).attr('data-html', 'true');
                $(input).tooltip('show');    
            }

        }

        // Úspešná správa
        if ('mail_sent' == event.detail.status) {
            $('.email-819').css('border', 'none');
            $('.email-819 input').hide();
            $('.wpcf7-not-valid-tip').hide();
            $('.wpcf7-submit').hide();
            $('.wpcf7-list-item').hide();
            var message = 'Boli ste prihlásený do newslettera.';
            //console.log(message);
            
            var response = '<div class="alert alert-success okto-success-message">' + message + '</div>';
            $('.email-819').append(response);
            $('.okto-success-message').fadeIn();
                                    
        }
        //console.log(event.detail.apiResponse.invalidFields[0].into);
        /*
        prilet
        .css('left', function () { return $(this).offset().left; })
                                    .animate({ "left": "0px" }, "slow");
                                    */
    }, false);

    /**************************
     * NEWSLETTER TRACKING CODE
     *************************/

    // nastav si div, kde sa formular nachadza (ak nezadas presne div, zoberie prvy CF7 formular)
    var newsletter = document.querySelector('.newsletter .wpcf7');
    if (newsletter !== null) {
        newsletter.addEventListener('wpcf7mailsent', function (event) {
            //ga('send', 'event', 'Contact Form', 'submit');
            gtag('event', 'Newsletter', {
                'event_label' : 'Newsletter',
            });
        }, false);
    }


    /**************************
    * POZICOVNA TRACKING CODE
    *************************/
    // nastav si div, kde sa formular nachadza (ak nezadas presne div, zoberie prvy CF7 formular)
    var pozicovna = document.querySelector('.pozicovna-mam-zaujem .wpcf7');
    if (pozicovna !== null) {
        pozicovna.addEventListener('wpcf7mailsent', function (event) {
            gtag('event', 'Pozicovna', {
                'event_label': 'Pozicovna'
            });
        }, false);
    }

    /**************************
     * KARTA VIP TRACKING CODE
     *************************/
    // nastav si div, kde sa formular nachadza (ak nezadas presne div, zoberie prvy CF7 formular)
    var karta_vip = document.querySelector('.karta-vip-modal .wpcf7');
    if (karta_vip !== null) {
        karta_vip.addEventListener('wpcf7mailsent', function (event) {
            gtag('event', 'Karta VIP', {
                'event_label': 'Karta VIP'
            });
        }, false);
    }

    /**************************
     * KARTA ZAKAZNIKA TRACKING CODE
     *************************/
    // nastav si div, kde sa formular nachadza (ak nezadas presne div, zoberie prvy CF7 formular)
    var karta_zakaznika = document.querySelector('.karta-zakaznika-modal .wpcf7');
    if (karta_zakaznika !== null) {
        karta_zakaznika.addEventListener('wpcf7mailsent', function (event) {
            gtag('event', 'Karta zakaznika', {
                'event_label': 'Karta zakaznika'
            });
        }, false);
    }

    /**************************
     * KARTA PARTNERA TRACKING CODE
     *************************/
    // nastav si div, kde sa formular nachadza (ak nezadas presne div, zoberie prvy CF7 formular)
    var karta_partnera = document.querySelector('.karta-partnera-modal .wpcf7');
    if (karta_partnera !== null) {
        karta_partnera.addEventListener('wpcf7mailsent', function (event) {
            gtag('event', 'Karta partnera', {
                'event_label': 'Karta partnera'
            });
        }, false);
    }


    /**************************
     * KONTAKTNY FORMULAR TRACKING CODE
     *************************/
    // nastav si div, kde sa formular nachadza (ak nezadas presne div, zoberie prvy CF7 formular)
    var contact_form = document.querySelector('.contact-block .wpcf7');
    if (contact_form !== null) {
        contact_form.addEventListener('wpcf7mailsent', function (event) {
            gtag('event', 'Kontaktny formular', {
                'event_label': 'Kontaktny formular'
            });
        }, false);
    }

});